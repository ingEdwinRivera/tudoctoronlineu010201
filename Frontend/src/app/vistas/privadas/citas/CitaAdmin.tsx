/* import { Link } from "react-router-dom";
import { useState, useContext, useEffect } from "react";
import { Button, Modal } from "react-bootstrap";

import Perfil from "../../../modelos/Perfil";
import Usuario from "../../../modelos/Usuario";
import Cita from "../../../modelos/Cita";
import { ContextoUsuario } from "../../../seguridad/ContextoUsuario";
import { ToastContainer } from "react-toastify";
import ApiBack from "../../../utilidades/dominios/ApiBack";
import ServicioPrivado from "../../../servicios/ServicioPrivado";
import { useFormulario } from "../../../utilidades/misHooks/useFormulario";
import { MensajeToastify } from "../../../utilidades/funciones/MensajeToastify";
import { obtenerFechaLocal, obtenerHora, } from "../../../utilidades/funciones/FormatoFecha";




export const CitaAdmin = () => {
  // Info del usuario en sesión
  const miUsuario = useContext(ContextoUsuario);
  const correoUsuario = String(miUsuario?.autenticado.correo);
  const codUsuario = String(miUsuario?.autenticado.codUsuario);
  // *******************************************************************

  // Esperar carga de datos
  const [todoListo, setTodoListo] = useState<boolean>(false);
  let cargaFinalizada = todoListo !== false;
  // *******************************************************************

  // ************************************************************************
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);

  const fechaVacia = new Date();
  const usuarioVacio = new Usuario( "", "", "", "", fechaVacia, 1, "", "", new Perfil("", "", 1) );

  type formaHtml = React.FormEvent<HTMLFormElement>;
  const [enProceso, setEnProceso] = useState<boolean>(false);
  const [arregloMedicos, setArregloMedicos] = useState<Usuario[]>([]);
  let { codMedico, dobleEnlace, objeto } = useFormulario<Cita>( new Cita(codUsuario, usuarioVacio, usuarioVacio, fechaVacia, 1) );

  //const [objUsu, setObjUsu] = useState<Cita>( new Cita("", "", "", "", new Date(), 1, "", "", new Perfil("", "", 1)) );
  const [arregloUsuarios, setArregloUsuarios] = useState<Usuario[]>([]);
  // ************************************************************************

  // Eliminar Cita
  // **************************************************************************
  const borrarCita = async (codigoCita: string) => {
    const urlBorrar = ApiBack.CITAS_ELIMINAR + "/" + codigoCita;
    const resultado = await ServicioPrivado.peticionDELETE(urlBorrar);
    console.log(resultado);
    if (typeof resultado.eliminado === "undefined") {
      MensajeToastify("error", "No se puede eliminar el usuario.", 7000);
    } else {
      MensajeToastify( "success", "Su cita ha sido eliminada con exito", 7000 );
    }
    obtenerCitas();
  };
  // **************************************************************************






    return (
      <main id="main" className="main">
        Componente Administrar cita
      </main>
    );
  };

  */ 

  export const CitaAdmin = () => {
    return (
      <main id="main" className="main">
        Componente Administrar cita
      </main>
    );
  };